package me.nipnacks.simplemessager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class messageCommand implements CommandExecutor {

    SimpleMessager plugin;

    public messageCommand(SimpleMessager plugin) {
        this.plugin = plugin;
    }
    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player && args.length > 0) {
            if (Bukkit.getOfflinePlayer(args[0]).getPlayer() != null) {
                Player messager = (Player) sender;
                Player reciever = Bukkit.getOfflinePlayer(args[0]).getPlayer();
                plugin.mc.setReplyTarget(messager, reciever);
                args [0] = "";
                String message = "";
                for (int i = 0; i < args.length; i++){
                    message += " " + args[i];
                }
                messager.sendMessage(ChatColor.GOLD  + "You whispered to " + reciever.getName() + ChatColor.YELLOW + " >>>" + ChatColor.DARK_AQUA + message);
                messager.getWorld().playSound(messager.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1, 1);
                reciever.sendMessage(ChatColor.GOLD  + messager.getName() + " Whispered to you " + ChatColor.YELLOW + ">>>" + ChatColor.DARK_AQUA + message);
                reciever.getWorld().playSound(reciever.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1, 1);
            } else {
                sender.sendMessage(ChatColor.RED + "The player you wish to message is not online!");
            }
        }





        return false;
    }
}
