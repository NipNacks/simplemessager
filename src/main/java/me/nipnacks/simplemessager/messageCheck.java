package me.nipnacks.simplemessager;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class messageCheck {

    SimpleMessager plugin;

    HashMap<Player,Player> conversation = new HashMap<Player, Player>();

    public messageCheck(SimpleMessager plugin) {
        this.plugin = plugin;
    }

    public void setReplyTarget(Player messager, Player reciever){
        conversation.put(messager, reciever);
        conversation.put(reciever,messager);
    }

    public Player getReplyTarget(Player messager){
        return conversation.get(messager);

    }
}
