package me.nipnacks.simplemessager;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class replyCommand implements CommandExecutor {

    SimpleMessager plugin;

    public replyCommand(SimpleMessager plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player messager = (Player) sender;
            if (plugin.mc.getReplyTarget(messager) == null){
                messager.sendMessage(ChatColor.RED + "You have no last conversation with other players!");
                return true;
            }
            Player reciever = plugin.mc.getReplyTarget(messager);
            String message = "";
            for (int i = 0; i < args.length; i++){
                message += " " + args[i];
            }
            messager.sendMessage(ChatColor.YELLOW  + "You whispered to >>>  " + reciever.getName() + ChatColor.DARK_AQUA + message);
            messager.getWorld().playSound(messager.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1, 1);
            reciever.sendMessage(ChatColor.YELLOW  + messager.getName() + "Whispered to you >>>  " + ChatColor.DARK_AQUA + message);
            reciever.getWorld().playSound(reciever.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1, 1);
            return true;
        }




        return false;
    }
}
