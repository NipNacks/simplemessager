package me.nipnacks.simplemessager;

import org.bukkit.plugin.java.JavaPlugin;

public final class SimpleMessager extends JavaPlugin {

    messageCheck mc;

    @Override
    public void onEnable() {
        // Plugin startup logic
    getLogger().info("SimpleMessager is now starting up!");
    saveConfig();
    getCommand("msg").setExecutor(new messageCommand(this));
    getCommand("r").setExecutor(new replyCommand(this));
    mc = new messageCheck(this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    getLogger().info("SimpleMessager is now shutting down!");
    }
}
